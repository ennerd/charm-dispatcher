<?php
declare(strict_types=1);

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/DummyRequestHandler.php';

$genericHandler = new DummyRequestHandler();

$dispatcher = new Charm\Dispatcher\CommandLineDispatcher($genericHandler);
$dispatcher->process();
