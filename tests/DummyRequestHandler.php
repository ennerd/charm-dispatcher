<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class DummyRequestHandler implements \Psr\Http\Server\RequestHandlerInterface
{
    public function __construct()
    {
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $factory = new Nyholm\Psr7\Factory\Psr17Factory();

        return $factory
            ->createResponse(200)
            ->withBody(
                $factory
                    ->createStream('Hello World from '.$request->getRequestTarget())
            );
    }
}
