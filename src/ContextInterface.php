<?php
declare(strict_types=1);

namespace Charm\Dispatcher;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Represents the context of a request-response cycle.
 */
interface ContextInterface
{
    /**
     * Get the currently active context object.
     */
    public static function instance(): ?self;

    /**
     * Get the server request we're currently processing.
     */
    public function getServerRequest(): ServerRequestInterface;

    /**
     * Send the response.
     */
    public function sendResponse(ResponseInterface $response);

    /**
     * Set context information.
     */
    public function __set($key, $value);

    /**
     * Get context information.
     */
    public function __get($key);

    public function __isset($key);

    public function __unset($key);
}
