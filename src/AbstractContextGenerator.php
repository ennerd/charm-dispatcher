<?php
declare(strict_types=1);

namespace Charm\Dispatcher;

use Iterator;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UploadedFileFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;

/**
 * A ContextGenerator receives requests and emits request-response context objects
 * which should be processed.
 *
 * @implements Iterator<ContextInterface>
 */
abstract class AbstractContextGenerator implements Iterator
{
    protected ServerRequestFactoryInterface $serverRequestFactory;
    protected UriFactoryInterface $uriFactory;
    protected UploadedFileFactoryInterface $uploadedFileFactory;
    protected StreamFactoryInterface $streamFactory;

    private $finished = false;
    private $queue = [];

    public function __construct(ServerRequestFactoryInterface $srf, UriFactoryInterface $uf, UploadedFileFactoryInterface $uff, StreamFactoryInterface $sf)
    {
        $this->serverRequestFactory = $srf;
        $this->uriFactory = $uf;
        $this->uploadedFileFactory = $uff;
        $this->streamFactory = $sf;
    }

    /**
     * Return an array of new ContextInterface instances. Return an empty array
     * if the queue is empty. Return null if there will be no more server requests.
     *
     * @return ?ContextInterface[]
     */
    abstract protected function getNewContexts(): ?array;

    /**
     * Get requests from the child class `getServerRequests()` method.
     */
    private function getSomeContexts(): void
    {
        // there won't be any more requests
        if ($this->finished) {
            return;
        }
        // no need to get some requests when we have a request to work with
        if (\array_key_exists(0, $this->queue)) {
            return;
        }
        $requests = $this->getNewContexts();
        if (null === $requests) {
            $this->finished = true;

            return;
        } else {
            foreach ($requests as $request) {
                $this->queue[] = $request;
            }
        }
    }

    /**
     * Returns true if the RequestGenerator is able to accept more
     * requests. This does not guarantee that more requests will arrive.
     */
    final public function valid(): bool
    {
        $this->getSomeContexts();

        return isset($this->queue[0]);
    }

    /**
     * Provide.
     */
    final public function current(): ?ContextInterface
    {
        $this->getSomeContexts();

        return $this->queue[0] ?? null;
    }

    /**
     * Get ready to process another request.
     */
    final public function next(): void
    {
        $this->getSomeContexts();
        array_shift($this->queue);
    }

    final public function key(): int
    {
        return 0;
    }

    final public function rewind(): void
    {
    }
}
