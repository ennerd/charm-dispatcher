<?php
declare(strict_types=1);

namespace Charm\Dispatcher;

use Nyholm\Psr7Server\ServerRequestCreator;

/**
 * Generate a ServerRequest from the command line based on the
 * parameters passed.
 */
class SAPIContextGenerator extends AbstractContextGenerator
{
    private bool $finished = false;

    public function getNewContexts(): ?array
    {
        if ($this->finished) {
            return null;
        }

        $creator = new ServerRequestCreator($this->serverRequestFactory, $this->uriFactory, $this->uploadedFileFactory, $this->streamFactory);
        $result = [
            new SAPIContext($creator->fromGlobals()),
        ];
        $this->finished = true;

        return $result;
    }
}
