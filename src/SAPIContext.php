<?php
declare(strict_types=1);

namespace Charm\Dispatcher;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * This class has borrowed heavily from laminas/httphandlerrunner.
 */
class SAPIContext extends AbstractContext
{
    private static ?ContextInterface $instance = null;
    private static ServerRequestInterface $request;
    private $maxBufferLength = 8192;

    public static function instance(): ?ContextInterface
    {
        return static::$instance;
    }

    public function __construct(ServerRequestInterface $request)
    {
        static::$instance = $this;
        $this->request = $request;
    }

    public function getServerRequest(): ServerRequestInterface
    {
        return $this->request;
    }

    public function sendResponse(ResponseInterface $response)
    {
        $this->assertNoPreviousOutput();
        $this->emitHeaders($response);
        $this->emitStatusLine($response);

        flush();

        // TODO This should be moved to a middleware
        $range = $this->parseContentRange($response->getHeaderLine('Content-Range'));

        if (null === $range || 'bytes' !== $range[0]) {
            $this->emitBody($response);

            return true;
        }

        $this->emitBodyRange($range, $response);
    }

    /**
     * Emit the status line.
     *
     * Emits the status line using the protocol version and status code from
     * the response; if a reason phrase is available, it, too, is emitted.
     *
     * It is important to mention that this method should be called after
     * `emitHeaders()` in order to prevent PHP from changing the status code of
     * the emitted response.
     *
     * @see \Laminas\HttpHandlerRunner\Emitter\SapiEmitterTrait::emitHeaders()
     */
    private function emitStatusLine(ResponseInterface $response): void
    {
        $reasonPhrase = $response->getReasonPhrase();
        $statusCode = $response->getStatusCode();

        header(sprintf(
            'HTTP/%s %d%s',
            $response->getProtocolVersion(),
            $statusCode,
            ($reasonPhrase ? ' '.$reasonPhrase : '')
        ), true, $statusCode);
    }

    /**
     * Emit response headers.
     *
     * Loops through each header, emitting each; if the header value
     * is an array with multiple values, ensures that each is sent
     * in such a way as to create aggregate headers (instead of replace
     * the previous).
     */
    private function emitHeaders(ResponseInterface $response): void
    {
        $statusCode = $response->getStatusCode();

        foreach ($response->getHeaders() as $header => $values) {
            $name = $this->filterHeader($header);
            $first = 'Set-Cookie' === $name ? false : true;
            foreach ($values as $value) {
                header(sprintf(
                    '%s: %s',
                    $name,
                    $value
                ), $first, $statusCode);
                $first = false;
            }
        }
    }

    /**
     * Filter a header name to wordcase.
     */
    private function filterHeader(string $header): string
    {
        return ucwords($header, '-');
    }

    /**
     * Emit the message body.
     */
    private function emitBody(ResponseInterface $response): void
    {
        $body = $response->getBody();

        if ($body->isSeekable()) {
            $body->rewind();
        }

        if (!$body->isReadable()) {
            echo $body;

            return;
        }

        while (!$body->eof()) {
            echo $body->read($this->maxBufferLength);
        }
    }

    /**
     * Emit a range of the message body.
     */
    private function emitBodyRange(array $range, ResponseInterface $response): void
    {
        list($unit, $first, $last, $length) = $range;

        $body = $response->getBody();

        $length = $last - $first + 1;

        if ($body->isSeekable()) {
            $body->seek($first);

            $first = 0;
        }

        if (!$body->isReadable()) {
            echo substr($body->getContents(), $first, $length);

            return;
        }

        $remaining = $length;

        while ($remaining >= $this->maxBufferLength && !$body->eof()) {
            $contents = $body->read($this->maxBufferLength);
            $remaining -= \strlen($contents);

            echo $contents;
        }

        if ($remaining > 0 && !$body->eof()) {
            echo $body->read($remaining);
        }
    }

    /**
     * Parse content-range header
     * http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.16.
     *
     * @return array|null [unit, first, last, length]; returns null if no
     *                    content range or an invalid content range is provided
     */
    private function parseContentRange(string $header): ?array
    {
        if (!preg_match('/(?P<unit>[\w]+)\s+(?P<first>\d+)-(?P<last>\d+)\/(?P<length>\d+|\*)/', $header, $matches)) {
            return null;
        }

        return [
            $matches['unit'],
            (int) $matches['first'],
            (int) $matches['last'],
            '*' === $matches['length'] ? '*' : (int) $matches['length'],
        ];
    }

    /**
     * Checks to see if content has previously been sent.
     *
     * If either headers have been sent or the output buffer contains content,
     * raises an exception.
     *
     * @throws EmitterException if headers have already been sent
     * @throws EmitterException if output is present in the output buffer
     */
    private function assertNoPreviousOutput(): void
    {
        if (headers_sent()) {
            throw new Error('Headers already sent');
        }

        if (ob_get_level() > 0 && ob_get_length() > 0) {
            throw new Error('Output buffer has data');
        }
    }
}
