<?php
declare(strict_types=1);

namespace Charm\Dispatcher;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class AbstractContext implements ContextInterface
{
    private $context = [];

    /**
     * Function returns the context that is currently being processed.
     */
    abstract public static function instance(): ?ContextInterface;

    /**
     * Function takes a response and sends it to the requester.
     */
    abstract public function sendResponse(ResponseInterface $response);

    abstract public function getServerRequest(): ServerRequestInterface;

    public function &__get($key)
    {
        if (\array_key_exists($key, $this->context)) {
            return $this->context[$key];
        }
        $result = null;

        return $result;
    }

    public function __set($key, $value)
    {
        $this->context[$key] = $value;
    }

    public function __unset($key)
    {
        unset($this->context[$key]);
    }

    public function __isset($key)
    {
        return isset($this->context[$key]);
    }
}
