<?php
declare(strict_types=1);

namespace Charm\Dispatcher;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class CliContext extends AbstractContext
{
    private static ?ContextInterface $instance = null;
    private static ServerRequestInterface $request;

    public static function instance(): ?ContextInterface
    {
        return static::$instance;
    }

    public function __construct(ServerRequestInterface $request)
    {
        static::$instance = $this;
        $this->request = $request;
    }

    public function getServerRequest(): ServerRequestInterface
    {
        return $this->request;
    }

    public function sendResponse(ResponseInterface $response)
    {
        flush();
        $body = $response->getBody();
        if ($body->isSeekable()) {
            $body->rewind();
        }

        if (!$body->isReadable()) {
            echo $body;

            return;
        }

        while (!$body->eof()) {
            echo $body->read(8192);
        }

        echo "\n";
    }
}
