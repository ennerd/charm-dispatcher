<?php
declare(strict_types=1);

namespace Charm\Dispatcher;

use StdClass;

/**
 * Generate a ServerRequest from the command line based on the
 * parameters passed.
 */
class CliContextGenerator extends AbstractContextGenerator
{
    private bool $finished = false;

    public function getNewContexts(): ?array
    {
        if ($this->finished) {
            return null;
        }
        $result = [
            $this->createContext(),
        ];
        $this->finished = true;

        return $result;
    }

    protected function createContext(): ContextInterface
    {
        $info = $this->parseCommandLine();
        $serverRequest = $this->serverRequestFactory->createServerRequest($info['method'], $info['uri'], $info['server']);
        foreach ($info['headers'] as $headerName => $headerValue) {
            $serverRequest = $serverRequest->withHeader($headerName, $headerValue);
        }

        return new CliContext($serverRequest);
    }

    /**
     * Create a new context from environment.
     */
    protected function parseCommandLine(): array
    {
        // need these:
        $method = 'GET';        // implemented for GET and POST
        $uri = null;            // implemented without HTTP_HOST
        $get = [];              // implemented
        $post = [];             // implemented
        $cookie = [];
        $input = null;          // refers to the input stream for put requests
        $files = [];
        /**
         * @psalm-suppress ForbiddenCode
         */
        $headers = [
            'Cache-Control' => 'no-cache',
            'Pragma' => 'no-cache',
            'User-Agent' => 'Charm CommandLineDispatcher/1.0 ('.trim(shell_exec('/usr/bin/uname -a') ?? '').')',
            'Connection' => 'Close',
        ];

        $ioModes = static::getIOModes();
        $stdinPost = null;      // holds post data from stdin, if received
        $stdinTaken = false;
        /**
         * Find $method, $path, $get, $post.
         */
        $argv = $GLOBALS['argv']; // this copies the $argv array, because PHP copies arrays on write

        // $path
        array_shift($argv);
        $pathParts = [];
        while (isset($argv[0])) {
            $part = array_shift($argv);
            if ('POST' === $part || '-' === substr($part, 0, 1)) {
                // put it back and break
                array_unshift($argv, $part);
                break;
            }
            $pathParts[] = $part;
        }
        $path = '/'.implode('/', $pathParts);

        // $get and $post
        $current = &$get;
        while (isset($argv[0])) {
            $part = array_shift($argv);

            if ('GET' === $method) {
                if ('POST' === $part) {
                    $method = 'POST';
                    $current = &$post;
                    continue;
                }
            }

            if ('--' === substr($part, 0, 2) && \strlen($part) > 2) { // parameters with double dashes
                $name = substr($part, 2);
                if (($p = strpos($name, '=')) !== false) { // parameter matching  --*=*
                    // argument in the form '--key=value'
                    $value = substr($name, $p + 1);
                    $name = substr($name, 0, $p);
                } elseif (isset($argv[0]) && ('-' === $argv[0] || '-' !== $argv[0][0])) { // parameter matching --* * (note! This means values can't start with -)
                    // argument in the form '--key value'
                    $value = array_shift($argv);
                } else {
                    // argument without value
                    $value = '1';
                }
                $current[$name][] = $value;
            } elseif ('-' === $part[0] && \strlen($part) > 1) {
                // single character arguments like -laF or -rm or -v or -?
                for ($i = 1; $i < \strlen($part); ++$i) {
                    $name = $part[$i];
                    $current[$name][] = '1';
                }
            } elseif ('-' === $part) {
                if (isset($argv[0])) {
                    throw new Error('Dash must be the last argument', 400);
                }
                $stdinTaken = true;
                if ('POST' === $method) {
                    // Parse post data from stdin
                    if ('tty' === $ioModes->in) {
                        throw new Error('Want to accept POST data via stdin because of a dash in the argument list, but nothing was piped', 400);
                    }
                    $stdinPost = $this->parseBuffer(stream_get_contents(\STDIN));
                } elseif ('GET' === $method) {
                    $method = 'PUT';
                }
            } else {
                throw new Error("I don't know how to treat '$part' from the command line", 400);
            }
        }

        if ('PUT' === $method) {
            $input = \STDIN;
        }

        foreach ($post as $name => &$values) {
            foreach ($values as $key => &$value) {
                if ('-' === $value) {
                    if ($stdinTaken) {
                        throw new Error("Can't use stdin via dash more than once", 400);
                    }
                    if ('tty' === $ioModes->in) {
                        throw new Error("Can's use stdin without a pipe", 400);
                    }
                    $stdinTaken = true;
                    $files[$name][] = $this->createUploadFromStream(\STDIN);
                    unset($values[$key]);
                } elseif (realpath($value)) {
                    exit("FILE UPLOAD SIMULATED: $value\n");
                }
            }
            if (1 === \count($values)) {
                $values = array_shift($values);
            } else {
                // in case some values were removed to $_FILES above
                $values = array_values($values);
            }
        }

        if (null !== $stdinPost) {
            $post = $post + $stdinPost;
        }

        if ('POST' === $method && \count($files) > 0) {
            $newFiles = [];
            foreach ($files as $name => $uploads) {
                foreach ($uploads as $upload) {
                    foreach ($upload as $field => $value) {
                        $newFiles[$name][$field][] = $value;
                    }
                }
            }

            foreach ($newFiles as $name => $fields) {
                foreach ($fields as $key => &$value) {
                    if (1 === \count($value)) {
                        $newFiles[$name][$key] = array_shift($value);
                    }
                }
            }
            $files = $newFiles;
        }

        $uri = $path;

        $query = [];
        if (\count($get) > 0) {
            /*
             * @psalm-suppress InvalidIterator
             */
            foreach ($get as $name => $vals) {
                if (1 === \count($vals)) {
                    $query[$name] = $vals[0];
                } else {
                    $query[$name] = $vals;
                }
            }
            $uri .= '?'.http_build_query($query);
        }

        $uri = $this->uriFactory->createUri($uri);

        $serverParams = $_SERVER;
        if ($sshClient = getenv('SSH_CLIENT')) {
            $parts = explode(' ', $sshClient);
            $serverParams['REMOTE_ADDR'] = $parts[0];
        } elseif ($sshConnection = getenv('SSH_CONNECTION')) {
            $parts = explode(' ', $sshConnection);
            $serverParams['REMOTE_ADDR'] = $parts[0];
        } else {
            $serverParams['REMOTE_ADDR'] = '127.0.0.1';
        }

        return [
            'method' => $method,
            'uri' => $uri,
            'get' => $query,
            'post' => $post,
            'cookie' => $cookie,
            'headers' => $headers,
            'server' => $serverParams,
            ];
    }

    protected function parseBuffer(string $buffer): ?array
    {
        $finfo = finfo_open(\FILEINFO_MIME);
        $mime = finfo_buffer($finfo, $buffer);
        $parts = explode(';', $mime);
        switch ($parts[0]) {
            case 'application/json':
                return json_decode($buffer, true);
                break;
            default:
                throw new Error("Can't parse buffer of type $mime", 406);
        }
    }

    protected static function getIOModes(): object
    {
        static $modes;

        if ($modes) {
            return $modes;
        }

        $ioMode = new class() {
            public mixed $stdin;
            public mixed $stdout;
            public mixed $stderr;

            private function getMode(&$dev, $fp)
            {
                $stat = fstat($fp);
                $mode = $stat['mode'] & 0170000; // S_IFMT

                $dev = new StdClass();
                $dev->isFifo = 0010000 == $mode; // S_IFIFO
                $dev->isChr = 0020000 == $mode; // S_IFCHR
                $dev->isDir = 0040000 == $mode; // S_IFDIR
                $dev->isBlk = 0060000 == $mode; // S_IFBLK
                $dev->isReg = 0100000 == $mode; // S_IFREG
                $dev->isLnk = 0120000 == $mode; // S_IFLNK
                $dev->isSock = 0140000 == $mode; // S_IFSOCK
                $dev->fstat = $stat;
            }

            public function __construct()
            {
                $this->getMode($this->stdin, \STDIN);
                $this->getMode($this->stdout, \STDOUT);
                $this->getMode($this->stderr, \STDERR);
            }
        };

        $modes = (object) [
            'out' => $ioMode->stdout->isChr ? 'tty' : ($ioMode->stdout->isFifo ? 'fifo' : ($ioMode->stdout->isReg ? 'file' : null)),
            'out_fstat' => $ioMode->stdout->fstat,
            'in' => $ioMode->stdin->isChr ? 'tty' : ($ioMode->stdin->isFifo ? 'fifo' : ($ioMode->stdin->isReg ? 'file' : ($ioMode->stdin->isDir ? 'dir' : null))),
            'in_fstat' => $ioMode->stdin->fstat,
            'err' => $ioMode->stderr->isChr ? 'tty' : ($ioMode->stderr->isFifo ? 'fifo' : ($ioMode->stderr->isReg ? 'file' : ($ioMode->stdin->isDir ? 'dir' : null))),
            'err_fstat' => $ioMode->stderr->fstat,
            ];

        return $modes;
    }

    /**
     * Create an array like $_FILES would contain.
     */
    protected function createUpload(): array
    {
        return [
            'name' => 'uploaded-file.bin',
            'type' => 'application/octet-stream',
            'tmp_name' => tempnam(sys_get_temp_dir(), 'chameleonUpload'),
            'error' => 0,
            'size' => 0,
            ];
    }

    /**
     * Create an array for a file stream resource.
     *
     * @param resource $stream
     */
    protected function createUploadFromStream($stream): array
    {
        $meta = stream_get_meta_data($stream);

        $result = $this->createUpload();

        $result['name'] = $meta['uri'];

        $fp = fopen($result['tmp_name'], 'wb+');
        $result['size'] = stream_copy_to_stream($stream, $fp);
        fclose($fp);
        register_shutdown_function(function () use ($result) {
            if (file_exists($result['tmp_name'])) {
                unlink($result['tmp_name']);
            }
        });

        $result['type'] = mime_content_type($result['tmp_name']);

        return $result;
    }
}
