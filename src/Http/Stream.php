<?php
namespace Charm\Dispatcher\Http;

use Generator;
use Charm\Dispatcher\Error;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use TypeError;

class Stream implements StreamInterface {

    const KIND_STRING = self::class.'::KIND_STRING';
    const KIND_RESOURCE = self::class.'::KIND_RESOURCE';
    const KIND_GENERATOR = self::class.'::KIND_GENERATOR';

    /**
     * Generators must yield on the first yield an array according to the following schema:
     * 
     * array(
     *   'seekable' => bool,    // if we accept seeking when we receive integer values from the yield keyword
     *   'size' => bool|null,   // if we know the size in advance
     * )
     * 
     * If the generator receives an integer, it should seek to the offset as specified by the integer.
     * `$received = yield $data; if (is_int($received)) { * seek ** }`
     *
     * Seeking is implemented by 
     */
    const GENERATOR_METADATA_SCHEMA = [
        'type' => 'object',
        'properties' => [
            'seekable' => [
                'type' => 'boolean',
                'default' => false,
            ],
            'size' => [
                'type' => 'int',
                'default' => null,
            ],
        ]
    ];

    const GENERATOR_DEFAULTS = [
        'seekable' => false,
        'size' => null,
    ];

    private string $kind;
    private mixed $source = null;
    private $buffer = '';
    private $bufferOffset = 0;
    private $closed = false;
    private $offset = 0;
    private ?array $generatorInfo = null;

    public function __construct($source) {
        if (is_callable($source)) {
            $source = $source();
        }
        if (is_resource($source)) {
            $this->kind = self::KIND_RESOURCE;
            $this->source = $source;
            return;
        } elseif (is_object($source)) {
            if ($source instanceof Generator) {
                $this->kind = self::KIND_GENERATOR;
                $this->source = $source;
                return;
            }
            if (method_exists($source, '__toString')) {
                $this->kind = self::KIND_RESOURCE;
                $this->source = fopen('php://memory', 'a+');
                fwrite($this->source, (string) $source);
                fseek($this->source, 0);
                return;
            }
            if (method_exists($source, 'jsonSerialize')) {
                $this->kind = self::KIND_RESOURCE;
                $this->source = fopen('php://memory', 'a+');
                fwrite($this->source, json_encode($source));
                fseek($this->source, 0);
                return;
            }
            throw new Error("Unsupported stream source of class '".get_class($source)."'");
        } elseif (is_string($source)) {
            $this->kind = self::KIND_RESOURCE;
            $this->source = fopen('php://memory', 'w+');
            fwrite($this->source, $source);
            fseek($this->source, 0);
            return;
        } else {
            throw new Error("Unsupported stream source of type '".gettype($source)."'");
        }
    }

    public function isSeekable() {
        $this->assertOpen();
        switch ($this->kind) {
            case self::KIND_RESOURCE :
                $metadata = \stream_get_meta_data($this->source);
                return $metadata['seekable'];
            case self::KIND_GENERATOR :
                $this->initGenerator();
                return $this->generatorInfo['seekable'];
            default :            
                $this->raiseNotImplementedError();
        }
    }

    public function isReadable() {
        $this->assertOpen();
        switch ($this->kind) {
            case self::KIND_RESOURCE :
                $metadata = \stream_get_meta_data($this->source);
                return strpos($metadata['mode'], 'r') !== false || strpos($metadata['mode'], 'w+') !== false || strpos($metadata['mode'], 'a+') !== false || strpos($metadata['mode'], 'x+') !== false || strpos($metadata['mode'], 'c+') !== false;
            case self::KIND_GENERATOR :
                return true;
            default :
                $this->raiseNotImplementedError();
        }
    }

    public function isWritable() {
        $this->assertOpen();
        switch ($this->kind) {
            case self::KIND_RESOURCE :
                $metadata = \stream_get_meta_data($this->source);
                return strpos($metadata['mode'], 'r+') !== false || strpos($metadata['mode'], 'w') !== false || strpos($metadata['mode'], 'a') !== false || strpos($metadata['mode'], 'x') !== false || strpos($metadata['mode'], 'x') !== false;
            case self::KIND_GENERATOR :
                return false;
            default :
                $this->raiseNotImplementedError();
        }
    }

    public function getSize() {
        $this->assertOpen();
        switch ($this->kind) {
            case self::KIND_RESOURCE :
                $stat = fstat($this->source);
                if (is_int($stat['size'])) {
                    return $stat['size'];
                } else {
                    return null;
                }
            case self::KIND_GENERATOR :
                $this->initGenerator();
                return $this->generatorInfo['size'];
        }
    }

    public function eof() {
        $this->assertOpen();
        switch ($this->kind) {
            case self::KIND_RESOURCE :
                return feof($this->source);
            case self::KIND_GENERATOR :
                $this->initGenerator();
                return !$this->source->valid();
            default :
                $this->raiseNotImplementedError();
        }
    }

    public function tell() {
        $this->assertOpen();
        switch ($this->kind) {
            case self::KIND_GENERATOR :
                return $this->offset;
            case self::KIND_RESOURCE :
                return ftell($this->source);
            default :
                $this->raiseNotImplementedError();
        }
    }

    public function rewind() {
        $this->assertOpen();
        if (!$this->isSeekable()) {
            throw new Error("Stream is not seekable");
        }
        $this->seek(0);
    }

    public function seek($offset, $whence = \SEEK_SET) {
        $this->assertOpen();
        $this->assertSeekable();
        switch ($this->kind) {
            case self::KIND_RESOURCE :
                fseek($this->source, $offset, $whence);
                break;
            case self::KIND_GENERATOR :
                $this->initGenerator();
                /**
                 * @var Generator
                 */
                $source = $this->source;
                if ($this->generatorInfo['seekable']) {
                    switch ($whence) {
                        case \SEEK_SET :
                            $this->offset = $offset;
                            $source->send((int) $offset);
                            break;
                        case \SEEK_CUR :
                            $this->offset += $offset;
                            $source->send((int) $this->offset);
                            break;
                        case \SEEK_END :
                            if ($this->generatorInfo['size'] !== null) {
                                $this->offset = $this->generatorInfo['size'] + $offset;
                                $source->send((int) $this->offset);
                            } else {
                                throw new Error("Can't seek using SEEK_END when size is unknown");
                            }
                            break;
                        default :
                            throw new Error("Invalid 'whence' specified in seek");
                    }
                }
                break;
            default :
                $this->raiseNotImplementedError();
        }
    }

    public function write($string) {
        $this->assertWritable();
        switch ($this->kind) {
            case self::KIND_RESOURCE :
                fwrite($this->source, $string);
                break;
            case self::KIND_GENERATOR :
                throw new \RuntimeException("Generator stream can't be written to");
            default :
                $this->raiseNotImplementedError();
        }
    }

    public function read($length) {
        $this->assertReadable();
        $this->assertInt($length);
        $length = max(0, $length);
        switch ($this->kind) {
            case self::KIND_RESOURCE :
                return fread($this->source, $length);
            case self::KIND_GENERATOR :
                $this->initGenerator();
                /**
                 * @var Generator
                 */
                $source = $this->source;
                if ($this->bufferOffset !== $this->offset) {
                    $this->bufferOffset = $this->offset;
                    $this->buffer = '';
                }
                if (strlen($this->buffer) < $length && $source->valid()) {
                    $chunk = $source->current();
                    $source->next();
                    if ($chunk !== null) {
                        $this->offset += strlen($chunk);
                        $this->buffer .= $chunk;
                    }
                }
                $result = substr($this->buffer, 0, $length);
                $resultLength = strlen($result);
                $this->bufferOffset += $resultLength;
                $this->buffer = substr($this->buffer, $resultLength);
                return $result;
            default :
                $this->raiseNotImplementedError();
        }
    }

    public function getContents() {
        $this->assertReadable();
        $result = '';
        while (!$this->eof()) {
            $result .= $this->read(65536);
        }
        return $result;
    }

    public function getMetadata($key = null) {
        $this->assertOpen();
        switch ($this->kind) {
            case self::KIND_RESOURCE :
                $result = stream_get_meta_data($this->source);
                break;
            case self::KIND_GENERATOR :
                $this->initGenerator();
                $result = [
                    'timed_out' => false,
                    'blocked' => false,
                    'eof' => $this->offset === strlen($this->source),
                    'unread_bytes' => 0,
                    'stream_type' => 'PHP generator',
                    'wrapper_type' => '',
                    'wrapper_data' => null,
                    'mode' => 'r',
                    'seekable' => $this->generatorInfo['seekable'],
                    'uri' => '',
                ];
                break;
            default :
                $this->raiseNotImplementedError();
        }

        if ($key === null) {
            return $result;
        }
        return $result[$key] ?? null;
    }

    public function detach() {
        $this->assertAttached();
        switch ($this->kind) {
            case self::KIND_STRING :
            case self::KIND_GENERATOR :
                $this->finishGenerator();
                $this->source = null;
                return null;
            case self::KIND_RESOURCE :
                $result = $this->source;
                $this->source = null;
                return $result;
            default :
                $this->raiseNotImplementedError();
        }
    }

    public function close() {
        $this->assertOpen();
        switch ($this->kind) {
            case self::KIND_STRING :
                $this->source = null;
                $this->closed = true;
                return;
            case self::KIND_RESOURCE :
                fclose($this->source);
                $this->closed = true;
                return;
            case self::KIND_GENERATOR :
                $this->initGenerator();
                $this->finishGenerator();
                $this->source = null;
                $this->closed = true;
                return;
            default :
                $this->raiseNotImplementedError();
        }
    }

    public function __toString() {
        try {
            switch ($this->kind) {
                case self::KIND_STRING :
                    $this->offset = strlen($this->source);
                    return $this->source;
                case self::KIND_RESOURCE :
                    if ($this->offset === 0) {
                        return stream_get_contents($this->source);
                    }
                    $this->rewind();
                    return stream_get_contents($this->source);
                case self::KIND_GENERATOR :
                    $this->initGenerator();
                    if (!$this->generatorInfo['bufferable']) {
                        throw new Error("Stream is not bufferable. It should be read in chunks.");
                    }

                    /**
                     * @var Generator
                     */
                    $source = $this->source;

                    if ($this->offset !== strlen($this->buffer)) {
                        if (!$this->generatorInfo['seekable']) {
                            throw new Error("Stream is not seekable and has been read from. It can't be cast to string.");
                        }
                        $this->seek(strlen($this->buffer));
                    }

                    while ($source->valid()) {
                        $chunk = $source->current();
                        $this->offset += strlen($chunk);
                        $this->buffer .= $chunk;
                    }
                    return $this->buffer;
                default :
                    $this->raiseNotImplementedError();
            }
        } catch (\Throwable $e) {
            return 'Error '.get_class($e).': '.$e->getMessage().' (code='.$e->getCode().')';
        }
    }

    private function initGenerator(): void {
        if (self::KIND_GENERATOR !== $this->kind) {
            throw new Error("Not using a generator");
        }
        if ($this->generatorInfo === null) {
            $result = $this->source->current();
            $this->source->next();
            if (!is_array($result)) {
                throw new Error("Generator streams must yield stream metadata according to '".self::class."::GENERATOR_METADATA_SCHEMA' on the first yield");
            }
            foreach (['seekable', 'size'] as $requiredKey) {
                if (!array_key_exists($requiredKey, $result)) {
                    throw new Error("Generator must yield key $requiredKey");
                }
            }
            $this->generatorInfo = $result;
        }
    }

    private function finishGenerator(): void {
        if (self::KIND_GENERATOR !== $this->kind) {
            throw new Error("Not using a generator");
        }
        /**
         * @var Generator
         */
        $source = $this->source;
        while ($source->valid()) {
            $source->current();
            $source->next();
        }
    }

    private function raiseNotImplementedError() {
        throw new Error("Not implemented");
    }

    private function assertInt($value) {
        if (!is_int($value)) {
            throw new TypeError("Expected integer value");
        }
    }

    private function assertString($value) {
        if (!is_string($value)) {
            throw new TypeError("Expected string value");
        }
    }

    private function assertReadable() {
        if (!$this->isReadable()) {
            throw new RuntimeException("Stream is not readable");
        }
    }

    private function assertWritable() {
        if (!$this->isWritable()) {
            throw new RuntimeException("Stream is not writable");
        }
    }

    private function assertSeekable() {
        $this->assertOpen();
        if (!$this->isSeekable()) {
            throw new RuntimeException("Stream is not seekable");
        }
    }

    private function assertAttached() {
        if ($this->source === null) {
            throw new RuntimeException("Stream is not in a usable state (detached)");
        }
    }

    private function assertOpen() {
        $this->assertAttached();
       if ($this->closed) {
            throw new RuntimeException("Stream is closed");
        }
    }
}