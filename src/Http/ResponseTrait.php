<?php
namespace Charm\Dispatcher\Http;

use Charm\Error\HttpCodes;

trait ResponseTrait {
    use MessageTrait;

    private int $statusCode = 200;
    private string $reasonPhrase = '';

    public function getStatusCode() {
        return $this->statusCode;
    }

    public function getReasonPhrase() {
        if ($this->reasonPhrase !== '') {
            return $this->reasonPhrase;
        }

        $code = $this->getStatusCode();
        if (isset(HttpCodes::PHRASES[$code])) {
            return HttpCodes::PHRASES[$code];
        }

        return '';
    }

    public function withStatus($code, $reasonPhrase = '') {
        $c = clone $this;
        $c->statusCode = $code;
        $c->reasonPhrase = $reasonPhrase;
        return $c;
    }
}