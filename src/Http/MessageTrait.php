<?php
namespace Charm\Dispatcher\Http;

use Charm\Loop\Promise;
use Generator;
use Psr\Http\Message\StreamInterface;

trait MessageTrait {
    private $headers = [];
    private string $protocolVersion = '1.1';
    private ?StreamInterface $body = null;

    /**
     * Generate data for the response.
     *
     * @return Promise|StreamInterface|Generator|string
     */
    abstract protected function getMessageBody();

    /**
     * Method is final in this trait to ensure immutability of messages. If
     * somebody uses self::withBody(), then we don't want to use the class
     * specific getBody() method.
     *
     * @return void
     */
    final public function getBody() {
        if ($this->body === null) {
            $this->body = new Stream($this->getMessageBody());
        }
        return $this->body;
    }

    public function withBody(StreamInterface $body) {
        $this->body = $body;
    }

    public function getProtocolVersion() {
        return $this->protocolVersion;
    }

    public function withProtocolVersion($version) {
        $c = clone $this;
        $c->protocolVersion = $version;
        return $c;
    }

    public function getHeaders() {
        return $this->headers;
    }

    public function hasHeader($name) {
        foreach ($this->getHeaders() as $headerName => $values) {
            if (strcasecmp($headerName, $name)) {
                return true;
            }
        }
        return false;
    }

    public function getHeader($name) {
        foreach ($this->getHeaders() as $headerName => $values) {
            if (strcasecmp($headerName, $name) === 0) {
                return $values;
            }
        }
        return [];
    }

    public function getHeaderLine($name) {
        $values = $this->getHeader($name);
        return implode(",", $values);
    }

    public function withoutHeader($name) {
        $c = clone $this;
        foreach ($c->headers as $headerName => $values) {
            if (strcasecmp($headerName, $name) === 0) {
                unset($c->headers[$headerName]);
                break;
            }
        }
        return $c;
    }

    public function withHeader($name, $value) {
        $c = $this->withoutHeader($name);
        $c->headers[$name] = is_array($value) ? $value : [ $value ];
        return $c;
    }

    public function withAddedHeader($name, $value) {
        $values = $this->getHeader($name);
        if (is_array($value)) {
            foreach ($value as $v) {
                $values[] = $v;
            }
        } else {
            $values[] = $value;
        }
        return $this->withHeader($name, $values);
    }
}