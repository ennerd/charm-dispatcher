<?php
declare(strict_types=1);

namespace Charm\Dispatcher;

use Psr\Http\Message\{ServerRequestFactoryInterface, UriFactoryInterface};
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Dispatcher that processes command line arguments to create a ServerRequest and dispatch
 * it to a RequestHandler.
 */
class SAPIDispatcher implements DispatcherInterface {

}
