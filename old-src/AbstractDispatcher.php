<?php
declare(strict_types=1);

namespace Charm\Dispatcher;

use Psr\Http\Message\{
    ServerRequestFactoryInterface,
    UriFactoryInterface,
    UploadedFileFactoryInterface,
    StreamFactoryInterface
};
use Psr\Http\Server\RequestHandlerInterface;

abstract class AbstractDispatcher implements DispatcherInterface {

    /**
     * The application that will handle all requests
     */
    protected RequestHandlerInterface $requestHandler;

    /**
     * Factories
     */
    protected ServerRequestFactoryInterface $serverRequestFactory;
    protected UriFactoryInterface $uriFactory;
    protected UploadedFileFactoryInterface $uploadedFileFactory;
    protected StreamFactoryInterface $streamFactory;

    public function __construct(
        RequestHandlerInterface $requestHandler,
        ServerRequestFactoryInterface $serverRequestFactory,
        UriFactoryInterface $uriFactory,
        UploadedFileFactoryInterface $uploadedFileFactory,
        StreamFactoryInterface $streamFactory)
    {
        $this->requestHandler = $requestHandler;
        $this->serverRequestFactory = $serverRequestFactory;
        $this->uriFactory = $uriFactory;
        $this->uploadedFileFactory = $uploadedFileFactory;
        $this->streamFactory = $streamFactory;
    }

}
