<?php

declare(strict_types=1);

use Nyholm\Psr7\ServerRequest;

class CliRequest extends ServerRequest
{
    public static function fromEnvironment(): self
    {
        $args = $GLOBALS['argv'];
        $paths = [];
        $opts = [];
        $command = array_shift($args);
        while (count($args) > 0) {
            $next = array_shift($args);
            if (strlen($next) > 2 && '-' === $next[0] && '-' === $next[1]) {
                $opt = explode('=', substr($next, 2));
                if (2 === count($opt)) {
                    $opts[$opt[0]] = $opt[1];
                } else {
                    $opts[$opt[0]] = 1;
                }
            } elseif (strlen($next) > 1 && '-' === $next[0]) {
                $length = strlen($next);
                for ($i = 1; $i < $length; ++$i) {
                    $opts[$next[$i]] = 1;
                }
            } else {
                $paths[] = $next;
            }
        }

        $path = implode('/', $paths);
        $data = null;

        return new static($path, $opts, $data);
    }

    public function respond($response): void
    {
        echo json_encode($response, \JSON_PRETTY_PRINT)."\n";
    }

    public function respondError(Throwable $error): void
    {
        echo 'Error: '.$error->getMessage()."\n".get_class($error)."\n";
    }
}
