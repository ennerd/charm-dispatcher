<?php
declare(strict_types=1);

namespace Charm\Dispatcher;

use Psr\Http\Message\{
    ServerRequestFactoryInterface,
    UriFactoryInterface,
    UploadedFileFactoryInterface,
    StreamFactoryInterface};
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class that can process requests in various contexts, to create
 * server requests and use the RequestHandler to handle the requests.
 */
interface DispatcherInterface {

    /**
     * @param $requestHandler
     */
    public function __construct(
        RequestHandlerInterface $requestHandler,
        ServerRequestFactoryInterface $serverRequestFactory,
        UriFactoryInterface $uriFactory,
        UploadedFileFactoryInterface $uploadedFileFactory,
        StreamFactoryInterface $streamFactory
    );

    /**
     * Start processing requests.
     */
    public function process(): void;

}
