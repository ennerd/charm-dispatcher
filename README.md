Charm Dispatcher
================

Create an application that implements `Psr\Http\Server\RequestHandlerInterface`, and run it:

```
    <?php
    use Psr\Http\Server\RequestHandlerInterface;
    use function Charm\serve;

    class HelloWorldApp implements RequestHandlerInterface {

        /**
         * Responds with "Hello World from /whatever/path"
         */
        public function handle(ServerRequestInterface $request): ResponseInterface {
            return $this->createResponse(200)->withBody($this->createStream("Hello World from ".$request->getRequestTarget()));
        }

    }

    // Serve via HTTP or command line
    serve(new MyApp());

```

Now you can access your application via the web and from the command line:

```
    # php myapp.php
    Hello World from /

    # php myapp.php users 123
    Hello World from /users/123

    # php myapp.php users --email "you@example.com"
    Hello World from /users?email=you@example.com

    # curl https://myserver.php/users?email=you@example.com
    Hello World from /users?email=you@example.com
```

